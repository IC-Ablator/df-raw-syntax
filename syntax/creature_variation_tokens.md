# Creature Variation Tokens

Creature variation tokens are used to create creatures which are derived from other
already-existing creatures without duplicating every single token.

Wiki: https://dwarffortresswiki.org/index.php/Creature_variation_token

TODO